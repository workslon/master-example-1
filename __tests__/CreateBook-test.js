var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');
var AppActions = require('../src/js/actions/AppActions');

jest.dontMock('../src/js/components/CreateBook.react');

describe('CreateBook.react', function () {
  var CreateBook = require('../src/js/components/CreateBook.react');
  var renderer = TestUtils.createRenderer();
  var result;
  var children;

  beforeEach(function () {
    renderer.render(<CreateBook />);
    result = renderer.getRenderOutput();
    children = result.props.children;
  });

  it('renders "CreateBook" view correctly', function () {
    // header
    expect(children[0].type).toEqual('h3');
    // ISBN label
    expect(children[1].props.children[0].type).toEqual('label');
    expect(children[1].props.children[0].props.children).toEqual('ISBN');
    // ISBN input
    expect(children[1].props.children[1].type).toEqual('input');
    expect(children[1].props.children[1].ref).toEqual('isbn');
    expect(children[1].props.children[1].props.defaultValue).toEqual('');
    expect(children[1].props.children[1].props.type).toEqual('text');
    // Title label
    expect(children[2].props.children[0].type).toEqual('label');
    expect(children[2].props.children[0].props.children).toEqual('Title');
    // Title input
    expect(children[2].props.children[1].type).toEqual('input');
    expect(children[2].props.children[1].ref).toEqual('title');
    expect(children[2].props.children[1].props.defaultValue).toEqual('');
    expect(children[2].props.children[1].props.type).toEqual('text');
    // Year label
    expect(children[3].props.children[0].type).toEqual('label');
    expect(children[3].props.children[0].props.children).toEqual('Year');
    // Year input
    expect(children[3].props.children[1].type).toEqual('input');
    expect(children[3].props.children[1].ref).toEqual('year');
    expect(children[3].props.children[1].props.defaultValue).toEqual('');
    expect(children[3].props.children[1].props.type).toEqual('text');
    // button
    expect(children[4].type).toEqual('button');
    expect(children[4].props.onClick).toBeDefined();
    expect(children[4].props.children).toEqual('Submit');
  });

  it('calls "AppActions.createBook" when clicking on "Submit" button', function () {
    // as the "createBook" function uses native "e.preventDefault" we need to simulate it.
    var fakeEvent = {
      preventDefault: function () {}
    };

    children[4].props.onClick(fakeEvent);
    expect(AppActions.createBook).toBeCalled();
  });
});