var React = require('react');
var ReactDOM = require('react-dom');
var AppActions = require('../actions/AppActions');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  componentDidMount: function () {
    ReactDOM.findDOMNode(this.refs.isbn).focus();
  },

  create: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var isbn = refs.isbn || {};
    var title = refs.title || {};
    var year = refs.year || {};

    AppActions.createBook({
      isbn: isbn.value,
      title: title.value,
      year: parseInt(year.value)
    });
  },

  render: function () {
    return (
      <form>
        <h3>Create Book</h3>
        <div className="form-group">
          <label htmlFor="isbn">ISBN</label>
          <input defaultValue="" ref="isbn" type="text" className="form-control" id="isbn" placeholder="ISBN" />
        </div>
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <input defaultValue="" ref="title" type="text" className="form-control" id="title" placeholder="Title" />
        </div>
        <div className="form-group">
          <label htmlFor="year">Year</label>
          <input defaultValue="" ref="year" type="text" className="form-control" id="year" placeholder="Year" />
        </div>
        <button type="submit" onClick={this.create} className="btn btn-default">Submit</button>
        <IndexLink className="back" to="/">&laquo; back</IndexLink>
      </form>
    );
  }
});