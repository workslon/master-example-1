var AppDispatcher = require('../dispatchers/AppDispatcher');
var XHR = require('../../../node_modules/xhr-parole');
var xhr = new XHR();

xhr.defaults = {
  url: 'https://public-library-api.herokuapp.com/api/classes/Book/',
  headers: {
    'X-Parse-Application-Id': 'MyH7zW1'
  }
};

module.exports = {
  getBooks: function () {
    var promise = xhr.GET();

    AppDispatcher.dispatchAsync(promise, {
      request: 'REQUEST_BOOKS',
      success: 'REQUEST_BOOKS_SUCCESS',
      failure: 'REQUEST_BOOKS_ERROR'
    });
  },

  createBook: function (data) {
    AppDispatcher.dispatchAsync(xhr.POST({data: data}), {
      request: 'REQUEST_BOOK_SAVE',
      success: 'BOOK_SAVE_SUCCESS',
      failure: 'BOOK_SAVE_ERROR'
    }, data);
  },

  updateBook: function (book, newData) {
    var promise = xhr.PUT({
      url: xhr.defaults.url + book.objectId,
      data: newData
    });

    newData.objectId = book.objectId;

    AppDispatcher.dispatchAsync(promise, {
      request: 'REQUEST_BOOK_UPDATE',
      success: 'BOOK_UPDATE_SUCCESS',
      failure: 'BOOK_UPDATE_ERROR'
    }, newData);
  },

  deleteBook: function (book) {
    var promise = xhr.DELETE({
      url: xhr.defaults.url + book.objectId
    });

    AppDispatcher.dispatchAsync(promise, {
      request: 'REQUEST_BOOK_DESTROY',
      success: 'BOOK_DESTROY_SUCCESS',
      failure: 'BOOK_DESTROY_ERROR'
    }, book)
  }
};